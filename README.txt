Notes/Questions regarding JELLO migrations as defined in the jello_migrate module.

MIGRATION CONTEXT:
There are a few circumstances we are taking into account in this migration.

0) for a variety of reasons organizationally, this first migration into drupal 7 is in the context of a two step
process which eventually leads to drupal 8, and so we are wanting to get things into place in a way that will maximally
 recycle when we are ready to move to drupal 8.

1) the field_bases and field_instances for all of JELLO's data structure / content types is currently featurized across
 a few different custom feature modules, the main of which are:
  a) jello_field_bases,
  b) jello_content, 
  c) jello_blog, 
  d) jello_publications_resources, and 
  e) jello_news_events.  

2) all permissions and roles are currently featurized as a group in the jello_roles_permissions feature.

MIGRATION GOAL:
0) the primary goal of this migration is to be able to install the site with a single script including a 'drush
site-install' command that points to a specific profile called 'jello'.  In addition to having an info file that names
specific module dependencies for the installation of the site (and installs them in perfect order first), there is also
an '.install' file that is able to take care of specific details of the installation and is custom but based on the
'standard' profile installation.

1) we are also using variety of modules and features that implement themselves when the jello_deploy module is enabled
using the hook_deploy_tools module - which will install the remainder of the modules needed after the migration.

2) so the goal is to use the 'jello' profile to first pull in and install relevant modules, before going through  the
processes defined in the '.install' file.

MIGRATION GROUPS
0) the migration is organized into the following groups listed here in the order of migration:
  a) jello-user: first we migrate the JelloRole class, and then we migrate the JelloUser class. JelloUser class has a
  dependency on JelloRole and must use information brought in with JelloRole.
  b) jello-file: the only class we are using for files is JelloFile.  
  c) jello-taxonomy: this is comprised of a variety of vocabularies - all of which are 'simple' in the sense that they
  do not have any fields associated with them.
  d) jello-taxonomy-complex: these are the 3 classes of taxonomies that have related fields associated with them.
  e) jello-content: this is the content that has taxonomy fields associated with it but no files that need to be
  assicaited with them.
  g) jello-content-complex: this is the content that has files associated with them and so refer to the JelloFile  class
   in some way.




