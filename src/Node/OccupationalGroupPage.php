<?php
/**
 * @file
 * News.
 */



/**
 * Migrate nodes to D7.
 */

class OccupationalGroupPage extends DrupalNode6Migration
{
  /**
   * Constructor override.
   */
  public function __construct(array $arguments)
  {
    
    $arguments['source_type'] = 'occupational_group_page';
    $arguments['destination_type'] = 'occupational_group_page';
    $arguments['machine_name'] = 'OccupationalGroupPage';
    
    parent::__construct($arguments);
  
    /**
     * OVO: the node egg.
     */
  
    $this->addUnmigratedSources(array('daycount', 'totalcount', 'timestamp'));
  
    $this->removeFieldMapping('body:language');
  
    $this->addFieldMapping('is_new', NULL, FALSE)->defaultValue(TRUE);
    $this->addFieldMapping('nid', 'nid', FALSE)
      ->defaultValue(1);
    $this->addFieldMapping('revision', 'revision', FALSE)
      ->defaultValue(1);
    $this->addFieldMapping('uid', 'uid', FALSE)->defaultValue(1);
    $this->addFieldMapping('revision_uid', 'revision_uid', FALSE);
    $this->addFieldMapping('log', 'log', FALSE)
      ->defaultValue(FALSE);
    $this->addFieldMapping('pathauto', NULL, FALSE)->defaultValue(0);
  
  
    /**
     * Now we are specific to the node.
     */
  
    $this->removeFieldMapping('body:format');
    
    // TAXONOMY: Dependencies
    $this->softDependencies = array('OccupationalGroups', 'PolicyAreas', 'Tags', 'Theme');
    
    $this->addFieldMapping('field_occupational_groups', 7)
      ->sourceMigration('OccupationalGroups');
    $this->addFieldMapping('field_occupational_groups:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_occupational_groups:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_occupational_groups:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_policy_areas', 6)
      ->sourceMigration('PolicyAreas');
    $this->addFieldMapping('field_policy_areas:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_policy_areas:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_policy_areas:ignore_case')->defaultValue(TRUE);
  
    $this->addFieldMapping('field_regions', 10)
      ->sourceMigration('Regions');
    $this->addFieldMapping('field_regions:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_regions:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_regions:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_tags', 8)
      ->sourceMigration('Tags');
    $this->addFieldMapping('field_tags:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_tags:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_tags:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_theme', 5)
      ->sourceMigration('Theme');
    $this->addFieldMapping('field_theme:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_theme:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_theme:ignore_case')->defaultValue(TRUE);
    
    //Text.
    $this->addFieldMapping('field_o_g_drive_force', 'field_o_g_drive_force');
    $this->addFieldMapping('field_o_g_drive_force:format', 'field_o_g_drive_force:format')->defaultValue('filtered_html');
    $this->addFieldMapping('field_o_g_org_voice', 'field_o_g_org_voice');
    $this->addFieldMapping('field_o_g_org_voice:format', 'field_o_g_org_voice:format')->defaultValue('filtered_html');
    $this->addFieldMapping('field_o_g_page', 'field_o_g_page');
    $this->addFieldMapping('field_o_g_page:format', 'field_o_g_page:format')->defaultValue('filtered_html');
    $this->addFieldMapping('field_o_g_policies_prog', 'field_o_g_policies_prog');
    $this->addFieldMapping('field_o_g_size_signif', 'field_o_g_size_signif');
    $this->addFieldMapping('field_o_g_policies_prog:format', 'field_o_g_policies_prog:format')->defaultValue('filtered_html');
    $this->addFieldMapping('field_o_g_size_signif:format', 'field_o_g_size_signif:format')->defaultValue('filtered_html');
    $this->addFieldMapping('field_page_extra', 'field_page_extra');
    $this->addFieldMapping('field_page_extra:format', 'field_page_extra:format')->defaultValue('filtered_html');
  
    // Images.
    $this->addFieldMapping('field_o_g_image', 'field_o_g_image')
      ->sourceMigration('JelloFile');
    $this->addFieldMapping('field_o_g_image:file_class')->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_o_g_image:preserve_files', NULL, FALSE)->defaultValue(TRUE);
    //$this->addFieldMapping('field_o_g_image:language')->defaultValue(LANGUAGE_NONE);
    $this->addFieldMapping('field_o_g_image:description', NULL)->defaultValue('Image');
    $this->addFieldMapping('field_o_g_image:display', NULL)->defaultValue(1);
  
    // Node Reference
    $this->addFieldMapping('field_related_resources', 'field_related_resources');
    $this->addFieldMapping('field_related_publications', 'field_related_publications');
    
  }
  
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    
    // Trim title length.
    if (strlen($row->title) > 254) {
      $tail = substr($row->title, 254);
      $row->title = substr($row->title, 0, 254);
      
      $message = "nid={$row->nid} Title shortened to '$row->title' (dropped '{$tail}').";
      $this->queueMessage($message);
    }
    
    // Retain nids as per:
    // https://drupal.stackexchange.com/questions/216855/preserve-nids-script-add-in-migrate-d2d
    
    // Fix formatting.
    if (!empty($row->format)) {
      if ($row->format == 4 || $row->format == 2) {
        $row->format = 'full_html';
      }
      if ($row->format == 3) { // This seems to be php.
        $row->format = 'plain_text';
      }
      if ($row->format == 1) {
        $row->format = 'filtered_html';
      }
    }
    
    // Not ready or necessary?
    try {
      $this->imageTagsToEmbedTags($row);
    } catch (Exception $e) {
    }
    
  }
  
  /**
   * Img tags embed.
   * @throws Exception
   */
  private function imageTagsToEmbedTags($row)
  {
    $body = $row->body;
    $type_detect = array(
      'UTF-8',
      'ASCII',
      'ISO-8859-1',
      'ISO-8859-2',
      'ISO-8859-3',
      'ISO-8859-4',
      'ISO-8859-5',
      'ISO-8859-6',
      'ISO-8859-7',
      'ISO-8859-8',
      'ISO-8859-9',
      'ISO-8859-10',
      'ISO-8859-13',
      'ISO-8859-14',
      'ISO-8859-15',
      'ISO-8859-16',
      'WINDOWS-1251',
      'Windows-1252',
      'Windows-1254',
    );
    $convert_from = mb_detect_encoding($body, $type_detect);
    if ($convert_from != 'UTF-8') {
      // This was not UTF-8 so report the anomaly.
      $message = "Converted from: {$convert_from}";
      $this->queueMessage($message);
    }
    
    $qp_options = array(
      'convert_to_encoding' => 'UTF-8',
      'convert_from_encoding' => $convert_from,
    );
    
    // Create query path object.
    try {
      $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
      $html .= '<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=';
      $html .= $convert_from;
      $html .= '" /></head><body>';
      $html .= $body;
      $html .= '</body></html>';
      $qp = htmlqp($html, NULL, $qp_options);
    } catch (Exception $e) {
      $error_message = $e->getMessage();
      $this->queueMessage("Failed instantiate QueryPath for HTML, Exception: {$error_message}");
    }
    
    if (!is_object($qp)) {
      throw new Exception("failed to initialize QueryPath");
    }
    
    // Non html content (plain text for example) does not get processed
    // correctly, so lets just return our body.
    $html = $qp->html();
    if (empty($html)) {
      $new_body = $body;
    } else {
      $new_body = $qp->find('body')->innerHtml();
    }
    
    $row->body = $new_body;
  }
  
  
}