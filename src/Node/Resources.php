<?php
/**
 * @file
 * News.
 */


/*namespace JelloMigrate\Node\Nodes;
/*use JelloMigrate\Node\Node;
/**
 * Migrate nodes to D7.
 */

class Resources extends DrupalNode6Migration
{
  /**
   * Constructor override.
   */
  public function __construct(array $arguments)
  {
    
    $arguments['source_type'] = 'resources';
    $arguments['destination_type'] = 'resources';
    $arguments['machine_name'] = 'Resources';
    
    parent::__construct($arguments);
  
    /**
     * OVO: the node egg.
     */
  
    $this->addUnmigratedSources(array('daycount', 'totalcount', 'timestamp'));
  
    $this->removeFieldMapping('body:language');
  
    $this->addFieldMapping('body:format', 'format', FALSE)->defaultValue('full_html');
    $this->addFieldMapping('is_new', NULL, FALSE)->defaultValue(TRUE);
    $this->addFieldMapping('nid', 'nid', FALSE)
      ->defaultValue(1);
    $this->addFieldMapping('revision', 'revision', FALSE)
      ->defaultValue(1);
    $this->addFieldMapping('uid', 'uid', FALSE)->defaultValue(1);
    $this->addFieldMapping('revision_uid', 'revision_uid', FALSE);
    $this->addFieldMapping('log', 'log', FALSE)
      ->defaultValue(FALSE);
    $this->addFieldMapping('pathauto', NULL, FALSE)->defaultValue(0);
  
  
    /**
     * Now we are specific to the node.
     */
  
  
  
    // TAXONOMY: Dependencies
    $this->softDependencies = array('Microsites', 'OccupationalGroups', 'PolicyAreas', 'Tags', 'Theme');
    
    $this->addFieldMapping('field_microsites', 21)
      ->sourceMigration('Microsites');
    $this->addFieldMapping('field_microsites:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_microsites:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_microsites:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_occupational_groups', 7)
      ->sourceMigration('OccupationalGroups');
    $this->addFieldMapping('field_occupational_groups:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_occupational_groups:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_occupational_groups:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_policy_areas', 6)
      ->sourceMigration('PolicyAreas');
    $this->addFieldMapping('field_policy_areas:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_policy_areas:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_policy_areas:ignore_case')->defaultValue(TRUE);
  
    $this->addFieldMapping('field_regions', 10)
      ->sourceMigration('Regions');
    $this->addFieldMapping('field_regions:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_regions:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_regions:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_tags', 8)
      ->sourceMigration('Tags');
    $this->addFieldMapping('field_tags:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_tags:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_tags:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_theme', 5)
      ->sourceMigration('Theme');
    $this->addFieldMapping('field_theme:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_theme:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_theme:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_informal_economy_topics', 20)
      ->sourceMigration('JelloTopics');
    $this->addFieldMapping('field_informal_economy_topics:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_informal_economy_topics:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_informal_economy_topics:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_language', 19)
      ->sourceMigration('Language');
    $this->addFieldMapping('field_language:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_language:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_language:ignore_case')->defaultValue(TRUE);
  
    $this->addFieldMapping('field_resource_type', 14)
      ->sourceMigration('ResourceTypes');
    $this->addFieldMapping('field_resource_type:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_resource_type:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_resource_type:ignore_case')->defaultValue(TRUE);
    
    
    // Links.
    $this->addFieldMapping('field_res_url', 'field_res_url');
    $this->addFieldMapping('field_res_url:title', 'field_res_url:title');
    $this->addFieldMapping('field_res_url:attributes', 'field_res_url:attributes');
    $this->addFieldMapping('field_res_url:language')->defaultValue('en');
    
    // Text.
    $this->addFieldMapping('field_res_embed_code', 'field_res_embed_code');
    $this->addFieldMapping('field_res_org_name', 'field_res_org_name');
    $this->addFieldMapping('field_res_site_name', 'field_res_site_name');
    $this->addFieldMapping('field_res_title', 'field_res_title');
    $this->addFieldMapping('field_res_video_title', 'field_res_video_title');
    $this->addFieldMapping('field_res_firstname', 'field_res_firstname');
    $this->addFieldMapping('field_res_lastname', 'field_res_lastname');
    
    // Computed Fields
    $this->addFieldMapping('field_sort_order', 'field_sort_order');
  
  
    // Images.
    $this->addFieldMapping('field_res_file', 'field_res_file')
      ->sourceMigration('JelloFile');
    $this->addFieldMapping('field_res_file:file_class')->defaultValue('MigrateFileFid');
    // TODO FILE
    $this->addFieldMapping('field_res_file:preserve_files', NULL, FALSE)->defaultValue(TRUE);
    //$this->addFieldMapping('field_res_file:language')->defaultValue(LANGUAGE_NONE);
    $this->addFieldMapping('field_res_file:description', NULL)->defaultValue('Image');
    $this->addFieldMapping('field_res_file:display', NULL)->defaultValue(1);
  
    // Images.
    $this->addFieldMapping('field_res_thumb_link', 'field_res_thumb_link')
      ->sourceMigration('JelloFile');
    $this->addFieldMapping('field_res_thumb_link:file_class')->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_res_thumb_link:preserve_files', NULL, FALSE)->defaultValue(TRUE);
    //$this->addFieldMapping('field_res_thumb_link:language')->defaultValue(LANGUAGE_NONE);
    $this->addFieldMapping('field_res_thumb_link:description', NULL)->defaultValue('Image');
    $this->addFieldMapping('field_res_thumb_link:display')->defaultValue(1);
  
  
    // Date.
    $this->addFieldMapping('field_res_date', 'field_res_date');
    $this->addFieldMapping('field_res_date:to')->defaultValue(0);
    $this->addFieldMapping('field_res_date:timezone')->defaultValue(0);
    $this->addFieldMapping('field_res_date:rrule')->defaultValue(0);
    
    // Other Taxonomy
    $this->addFieldMapping('field_pub_featured', 'field_pub_featured');
    $this->addFieldMapping('field_pub_featured:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_pub_featured:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_pub_featured:ignore_case')->defaultValue(TRUE);
    
    
    $this->addFieldMapping('field_resource_type', 14, FALSE)
      ->sourceMigration('ResourceTypes');
    //$this->addFieldMapping('field_resource_type:source_type')->defaultValue('tid');
    //$this->addFieldMapping('field_resource_type:create_term')->defaultValue(TRUE);
    //$this->addFieldMapping('field_resource_type:ignore_case')->defaultValue(TRUE);
    
    
  }
  
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    
    // Translate images
    $file_fids = array();
    if (isset($row->field_res_file) && is_array($row->field_res_file)) {
      foreach($row->field_res_file as $delta => $fid) {
        $file_fids[] = $fid;
      }
    }
    else {
      if (isset($row->field_res_file)) {
        $file_fids[] = $row->field_res_file;
      }
    }
    // Translate images
    $res_thumb_fids = array();
    if (isset($row->field_res_thumb_link) && is_array($row->field_res_thumb_link)) {
      foreach($row->field_res_thumb_link as $delta => $fid) {
        $res_thumb_fids[] = $fid;
      }
    }
    else {
      if (isset($row->field_res_thumb_link)) {
        $res_thumb_fids[] = $row->field_res_thumb_link;
      }
    }
    $row->file_fids = $file_fids;
    $row->res_thumb_fids = $res_thumb_fids;
    
    $query = db_select('jello.' . 'term_node', 't')
      ->fields('t', array('tid', 'vid', 'nid'))
      ->condition('t.vid', 10, '=')
      ->condition('t.nid', $row->nid, '=');
    $results = $query->execute();
  
    $regions = array();
    if (isset($results)) {
      foreach ($results as $result) {
        $regions[] = $result->tid;
      }
    }
    $row->regions = $regions;
    
    // Trim title length.
    if (strlen($row->title) > 254) {
      $tail = substr($row->title, 254);
      $row->title = substr($row->title, 0, 254);
      
      $message = "nid={$row->nid} Title shortened to '$row->title' (dropped '{$tail}').";
      $this->queueMessage($message);
    }
    
    // Retain nids as per:
    // https://drupal.stackexchange.com/questions/216855/preserve-nids-script-add-in-migrate-d2d
    
    // Fix formatting.
    if (!empty($row->format)) {
      if ($row->format == 4 || $row->format == 2) {
        $row->format = 'full_html';
      }
      if ($row->format == 3) { // This seems to be php.
        $row->format = 'plain_text';
      }
      if ($row->format == 1) {
        $row->format = 'filtered_html';
      }
    }
    
    // Not ready or necessary?
    try {
      $this->imageTagsToEmbedTags($row);
    } catch (Exception $e) {
    }
    
  }
  
  /**
   * Img tags embed.
   * @throws Exception
   */
  private function imageTagsToEmbedTags($row)
  {
    $body = $row->body;
    $type_detect = array(
      'UTF-8',
      'ASCII',
      'ISO-8859-1',
      'ISO-8859-2',
      'ISO-8859-3',
      'ISO-8859-4',
      'ISO-8859-5',
      'ISO-8859-6',
      'ISO-8859-7',
      'ISO-8859-8',
      'ISO-8859-9',
      'ISO-8859-10',
      'ISO-8859-13',
      'ISO-8859-14',
      'ISO-8859-15',
      'ISO-8859-16',
      'WINDOWS-1251',
      'Windows-1252',
      'Windows-1254',
    );
    $convert_from = mb_detect_encoding($body, $type_detect);
    if ($convert_from != 'UTF-8') {
      // This was not UTF-8 so report the anomaly.
      $message = "Converted from: {$convert_from}";
      $this->queueMessage($message);
    }
    
    $qp_options = array(
      'convert_to_encoding' => 'UTF-8',
      'convert_from_encoding' => $convert_from,
    );
    
    // Create query path object.
    try {
      $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
      $html .= '<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=';
      $html .= $convert_from;
      $html .= '" /></head><body>';
      $html .= $body;
      $html .= '</body></html>';
      $qp = htmlqp($html, NULL, $qp_options);
    } catch (Exception $e) {
      $error_message = $e->getMessage();
      $this->queueMessage("Failed instantiate QueryPath for HTML, Exception: {$error_message}");
    }
    
    if (!is_object($qp)) {
      throw new Exception("failed to initialize QueryPath");
    }
    
    // Non html content (plain text for example) does not get processed
    // correctly, so lets just return our body.
    $html = $qp->html();
    if (empty($html)) {
      $new_body = $body;
    } else {
      $new_body = $qp->find('body')->innerHtml();
    }
    
    $row->body = $new_body;
  }
  
  
}