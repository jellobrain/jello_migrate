<?php
/**
 * @file
 * OrgRepDb.
 */



/**
 * Migrate nodes to D7.
 */

class OrgRepDb extends DrupalNode6Migration
{
  /**
   * Constructor override.
   */
  public function __construct(array $arguments)
  {
    
    $arguments['source_type'] = 'org_rep_db';
    $arguments['destination_type'] = 'org_rep_db';
    $arguments['machine_name'] = 'OrgRepDb';
    
    parent::__construct($arguments);
  
    /**
     * OVO: the node egg.
     */
  
    $this->addUnmigratedSources(array('daycount', 'totalcount', 'timestamp'));
  
    $this->removeFieldMapping('body:language');
  
    $this->addFieldMapping('is_new', NULL, FALSE)->defaultValue(TRUE);
    $this->addFieldMapping('nid', 'nid', FALSE)
      ->defaultValue(1);
    $this->addFieldMapping('revision', 'revision', FALSE)
      ->defaultValue(1);
    $this->addFieldMapping('uid', 'uid', FALSE)->defaultValue(1);
    $this->addFieldMapping('revision_uid', 'revision_uid', FALSE);
    $this->addFieldMapping('log', 'log', FALSE)
      ->defaultValue(FALSE);
    $this->addFieldMapping('pathauto', NULL, FALSE)->defaultValue(0);
  
  
    /**
     * Now we are specific to the node.
     */
  
    $this->removeFieldMapping('body:format');
    
    //Text.
    $this->addFieldMapping('field_case_study_links', 'field_case_study_links');
    $this->addFieldMapping('field_education_training', 'field_education_training');
    $this->addFieldMapping('field_info_source', 'field_info_source');
    $this->addFieldMapping('field_key_services', 'field_key_services');
    $this->addFieldMapping('field_members_occupation_details', 'field_members_occupation_details');
    $this->addFieldMapping('field_negociations_agreements', 'field_negotiations_agreements');
    $this->addFieldMapping('field_objectives', 'field_objectives');
    $this->addFieldMapping('field_org_structure', 'field_org_structure');
    $this->addFieldMapping('field_other_info', 'field_other_info');
    $this->addFieldMapping('field_strategy', 'field_strategy');
    
    $this->addFieldMapping('field_address', 'field_address');
    $this->addFieldMapping('field_affiliations', 'field_affilliations');
    $this->addFieldMapping('field_city', 'field_city');
    $this->addFieldMapping('field_date_updated', 'field_date_updated');
    $this->addFieldMapping('field_email_address', 'field_email_address');
    $this->addFieldMapping('field_entry_date', 'field_entry_date');
    $this->addFieldMapping('field_fax', 'field_fax');
    $this->addFieldMapping('field_gender_comp', 'field_gender_comp');
    $this->addFieldMapping('field_ie_members', 'field_ie_members');
    $this->addFieldMapping('field_members_workplace', 'field_members_workplace');
    $this->addFieldMapping('field_org_scope', 'field_org_scope');
    $this->addFieldMapping('field_org_type', 'field_org_type');
    $this->addFieldMapping('field_phone', 'field_phone');
    $this->addFieldMapping('field_primary_contact', 'field_primary_contact');
    $this->addFieldMapping('field_secondary_contact', 'field_secondary_contact');
    $this->addFieldMapping('field_website', 'field_website');
    $this->addFieldMapping('field_ord_language', 'field_language');
    $this->addFieldMapping('field_ord_region', 'field_region');
    
    // TAXONOMY: Dependencies
    $this->softDependencies = array('Microsites');
    
    $this->addFieldMapping('field_microsites', 21)
      ->sourceMigration('Microsites');
    $this->addFieldMapping('field_microsites:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_microsites:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_microsites:ignore_case')->defaultValue(TRUE);
    
    
    $this->addFieldMapping('field_members_occupation', 'field_members_occupation');
    $this->addFieldMapping('field_womens_org', 'field_womens_org');
    $this->addFieldMapping('field_country', 'field_country');
    
    
  }
  
  /**
   * Prepare each row for import.
   *
   * Use to lazy create the topic vocabulary.
   * Can be extended to clean the body text or for regions and countries.
   */
  public function prepareRow($row)
  {
    parent::prepareRow($row);
    
    
    // Trim title length.
    if (strlen($row->title) > 254) {
      $tail = substr($row->title, 254);
      $row->title = substr($row->title, 0, 254);
      
      $message = "nid={$row->nid} Title shortened to '$row->title' (dropped '{$tail}').";
      $this->queueMessage($message);
    }
    
    // Retain nids as per:
    // https://drupal.stackexchange.com/questions/216855/preserve-nids-script-add-in-migrate-d2d
    
    // Fix formatting.
    if (!empty($row->format)) {
      if ($row->format == 4 || $row->format == 2) {
        $row->format = 'full_html';
      }
      if ($row->format == 3) { // This seems to be php.
        $row->format = 'plain_text';
      }
      if ($row->format == 1) {
        $row->format = 'filtered_html';
      }
    }
    
    // Not ready or necessary?
    try {
      $this->imageTagsToEmbedTags($row);
    } catch (Exception $e) {
    }
    
  }
  
  /**
   * Img tags embed.
   * @throws Exception
   */
  private function imageTagsToEmbedTags($row)
  {
    $body = $row->body;
    $type_detect = array(
      'UTF-8',
      'ASCII',
      'ISO-8859-1',
      'ISO-8859-2',
      'ISO-8859-3',
      'ISO-8859-4',
      'ISO-8859-5',
      'ISO-8859-6',
      'ISO-8859-7',
      'ISO-8859-8',
      'ISO-8859-9',
      'ISO-8859-10',
      'ISO-8859-13',
      'ISO-8859-14',
      'ISO-8859-15',
      'ISO-8859-16',
      'WINDOWS-1251',
      'Windows-1252',
      'Windows-1254',
    );
    $convert_from = mb_detect_encoding($body, $type_detect);
    if ($convert_from != 'UTF-8') {
      // This was not UTF-8 so report the anomaly.
      $message = "Converted from: {$convert_from}";
      $this->queueMessage($message);
    }
    
    $qp_options = array(
      'convert_to_encoding' => 'UTF-8',
      'convert_from_encoding' => $convert_from,
    );
    
    // Create query path object.
    try {
      $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
      $html .= '<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=';
      $html .= $convert_from;
      $html .= '" /></head><body>';
      $html .= $body;
      $html .= '</body></html>';
      $qp = htmlqp($html, NULL, $qp_options);
    } catch (Exception $e) {
      $error_message = $e->getMessage();
      $this->queueMessage("Failed instantiate QueryPath for HTML, Exception: {$error_message}");
    }
    
    if (!is_object($qp)) {
      throw new Exception("failed to initialize QueryPath");
    }
    
    // Non html content (plain text for example) does not get processed
    // correctly, so lets just return our body.
    $html = $qp->html();
    if (empty($html)) {
      $new_body = $body;
    } else {
      $new_body = $qp->find('body')->innerHtml();
    }
    
    $row->body = $new_body;
  }
  
  
}