<?php
/**
 * @file
 * Specialist.
 */



/**
 * Migrate nodes to D7.
 */

class Specialist extends DrupalNode6Migration
{
  /**
   * Constructor override.
   */
  public function __construct(array $arguments)
  {
    
    $arguments['source_type'] = 'specialist';
    $arguments['destination_type'] = 'specialist';
    $arguments['machine_name'] = 'Specialist';
    
    parent::__construct($arguments);
    
    /**
     * OVO: the node egg.
     */
  
    $this->addUnmigratedSources(array('daycount', 'totalcount', 'timestamp'));
    
    $this->removeFieldMapping('body:language');
  
    $this->addFieldMapping('body:format', 'format', FALSE)->defaultValue('full_html');
    $this->addFieldMapping('is_new', NULL, FALSE)->defaultValue(TRUE);
    $this->addFieldMapping('nid', 'nid', FALSE)
      ->defaultValue(1);
    $this->addFieldMapping('revision', 'revision', FALSE)
      ->defaultValue(1);
    $this->addFieldMapping('uid', 'uid', FALSE)->defaultValue(1);
    $this->addFieldMapping('revision_uid', 'revision_uid', FALSE);
    $this->addFieldMapping('log', 'log', FALSE)
      ->defaultValue(FALSE);
    $this->addFieldMapping('pathauto', NULL, FALSE)->defaultValue(0);
  
    
    /**
     * Now we are specific to the node.
     */
    
    $this->addFieldMapping('body:summary', 'teaser', FALSE)->defaultValue('');
    
    
    // Text.
    $this->addFieldMapping('field_additional_info', 'field_additional_info');
    $this->addFieldMapping('field_additional_info:format', 'field_additional_info:format');
  
    $this->addFieldMapping('field_specialist_publications', 'field_specialist_publications')->defaultValue('tid');
    
  
  
    // Images Test 1 - standard in use.
    $this->addFieldMapping('field_author_image', 'field_author_image')
      ->sourceMigration('JelloFile');
    $this->addFieldMapping('field_author_image:file_class')->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_author_image:preserve_files', NULL, FALSE)->defaultValue(TRUE);
    //$this->addFieldMapping('field_author_image:language')->defaultValue(LANGUAGE_NONE);
    $this->addFieldMapping('field_author_image:alt', NULL)->defaultValue('Image');
    $this->addFieldMapping('field_author_image:title', NULL)->defaultValue('Image');
  
  }
  
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    // Translate images
    $fids = array();
    if (isset($row->field_author_image)) {
      foreach($row->field_author_image as $delta => $fid) {
        $fids[] = $fid;
      }
    }
    
    
    /*if (!empty($fids)) {
      foreach ($fids as $key => $fid) {
        $query = db_insert('field_data_field_author_image')
          ->fields(array(
            'entity_type' => 'node',
            'bundle' => 'specialist',
            'deleted' => 0,
            'entity_id' => $row->nid,
            'revision_id' => $row->nid,
            'delta' => 0,
            'field_author_image_fid' => $fid,
            'field_author_image_alt' => NULL,
            'field_author_image_title' => NULL,
            'field_author_image_width' => NULL,
            'field_author_image_height' => NULL,
          ))
          ->execute();
      }
    }*/
  
    $row->fids = $fids;

    
    // Trim title length.
    if (strlen($row->title) > 254) {
      $tail = substr($row->title, 254);
      $row->title = substr($row->title, 0, 254);
      
      $message = "nid={$row->nid} Title shortened to '$row->title' (dropped '{$tail}').";
      $this->queueMessage($message);
    }
    
    // Retain nids as per:
    // https://drupal.stackexchange.com/questions/216855/preserve-nids-script-add-in-migrate-d2d
    
    // Fix formatting.
    if (!empty($row->format)) {
      if ($row->format == 4 || $row->format == 2) {
        $row->format = 'full_html';
      }
      if ($row->format == 3) { // This seems to be php.
        $row->format = 'plain_text';
      }
      if ($row->format == 1) {
        $row->format = 'filtered_html';
      }
    }
    
    // Not ready or necessary?
    try {
      $this->imageTagsToEmbedTags($row);
    } catch (Exception $e) {
    }
    
  }
  
  /**
   * Img tags embed.
   * @throws Exception
   */
  private function imageTagsToEmbedTags($row)
  {
    $body = $row->body;
    $type_detect = array(
      'UTF-8',
      'ASCII',
      'ISO-8859-1',
      'ISO-8859-2',
      'ISO-8859-3',
      'ISO-8859-4',
      'ISO-8859-5',
      'ISO-8859-6',
      'ISO-8859-7',
      'ISO-8859-8',
      'ISO-8859-9',
      'ISO-8859-10',
      'ISO-8859-13',
      'ISO-8859-14',
      'ISO-8859-15',
      'ISO-8859-16',
      'WINDOWS-1251',
      'Windows-1252',
      'Windows-1254',
    );
    $convert_from = mb_detect_encoding($body, $type_detect);
    if ($convert_from != 'UTF-8') {
      // This was not UTF-8 so report the anomaly.
      $message = "Converted from: {$convert_from}";
      $this->queueMessage($message);
    }
    
    $qp_options = array(
      'convert_to_encoding' => 'UTF-8',
      'convert_from_encoding' => $convert_from,
    );
    
    // Create query path object.
    try {
      $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
      $html .= '<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=';
      $html .= $convert_from;
      $html .= '" /></head><body>';
      $html .= $body;
      $html .= '</body></html>';
      $qp = htmlqp($html, NULL, $qp_options);
    } catch (Exception $e) {
      $error_message = $e->getMessage();
      $this->queueMessage("Failed instantiate QueryPath for HTML, Exception: {$error_message}");
    }
    
    if (!is_object($qp)) {
      throw new Exception("failed to initialize QueryPath");
    }
    
    // Non html content (plain text for example) does not get processed
    // correctly, so lets just return our body.
    $html = $qp->html();
    if (empty($html)) {
      $new_body = $body;
    } else {
      $new_body = $qp->find('body')->innerHtml();
    }
    
    $row->body = $new_body;
  }
  
}