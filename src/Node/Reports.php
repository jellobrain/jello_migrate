<?php
/**
 * @file
 * Reports.
 */



/**
 * Migrate nodes to D7.
 */

class Reports extends DrupalNode6Migration
{
  /**
   * Constructor override.
   */
  public function __construct(array $arguments)
  {
    
    $arguments['source_type'] = 'reports';
    $arguments['destination_type'] = 'reports';
    $arguments['machine_name'] = 'Reports';
    
    parent::__construct($arguments);
  
    /**
     * OVO: the node egg.
     */
  
    $this->addUnmigratedSources(array('daycount', 'totalcount', 'timestamp'));
  
    $this->removeFieldMapping('body:language');
  
    $this->addFieldMapping('body:format', 'format', FALSE)->defaultValue('full_html');
    $this->addFieldMapping('is_new', NULL, FALSE)->defaultValue(TRUE);
    $this->addFieldMapping('nid', 'nid', FALSE)
      ->defaultValue(1);
    $this->addFieldMapping('revision', 'revision', FALSE)
      ->defaultValue(1);
    $this->addFieldMapping('uid', 'uid', FALSE)->defaultValue(1);
    $this->addFieldMapping('revision_uid', 'revision_uid', FALSE);
    $this->addFieldMapping('log', 'log', FALSE)
      ->defaultValue(FALSE);
    $this->addFieldMapping('pathauto', NULL, FALSE)->defaultValue(0);
  
  
    /**
     * Now we are specific to the node.
     */
  
  
  
  
    // TAXONOMY: Dependencies
    $this->softDependencies = array('Microsites', 'OccupationalGroups', 'PolicyAreas', 'Tags', 'Theme');
    
    $this->addFieldMapping('field_microsites', 21)
      ->sourceMigration('Microsites');
    $this->addFieldMapping('field_microsites:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_microsites:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_microsites:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_occupational_groups', 7)
      ->sourceMigration('OccupationalGroups');
    $this->addFieldMapping('field_occupational_groups:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_occupational_groups:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_occupational_groups:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_policy_areas', 6)
      ->sourceMigration('PolicyAreas');
    $this->addFieldMapping('field_policy_areas:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_policy_areas:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_policy_areas:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_regions', 10)
      ->sourceMigration('Regions');
    $this->addFieldMapping('field_regions:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_regions:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_regions:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_tags', 8)
      ->sourceMigration('Tags');
    $this->addFieldMapping('field_tags:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_tags:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_tags:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_theme', 5)
      ->sourceMigration('Theme');
    $this->addFieldMapping('field_theme:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_theme:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_theme:ignore_case')->defaultValue(TRUE);
    
    $this->addFieldMapping('field_language', 19)
      ->sourceMigration('Language');
    $this->addFieldMapping('field_language:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_language:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_language:ignore_case')->defaultValue(TRUE);
    
    
    // Links.
    $this->addFieldMapping('field_rep_url', 'field_rep_url');
    $this->addFieldMapping('field_rep_url:title', 'field_rep_url:title');
    $this->addFieldMapping('field_rep_url:attributes', 'field_rep_url:attributes');
    $this->addFieldMapping('field_rep_url:language')->defaultValue('und');
  
    // Images.
    $this->addFieldMapping('field_rep_file', 'field_rep_file')
      ->sourceMigration('JelloFile');
    $this->addFieldMapping('field_rep_file:file_class')->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_rep_file:preserve_files', NULL, FALSE)->defaultValue(TRUE);
    //$this->addFieldMapping('field_rep_file:language')->defaultValue(LANGUAGE_NONE);
    $this->addFieldMapping('field_rep_file:description', NULL)->defaultValue('Image');
    $this->addFieldMapping('field_rep_file:display', NULL)->defaultValue(1);
  
  
  
    // Other.
    $this->addFieldMapping('field_rep_firstname', 'field_rep_firstname');
    $this->addFieldMapping('field_rep_lastname', 'field_rep_lastname');
    $this->addFieldMapping('field_sort_order', 'field_sort_order');
    $this->addFieldMapping('field_pub_featured', 'field_pub_featured');
    $this->addFieldMapping('field_pub_featured:source_type')->defaultValue('tid');
    $this->addFieldMapping('field_pub_featured:create_term')->defaultValue(TRUE);
    $this->addFieldMapping('field_pub_featured:ignore_case')->defaultValue(TRUE);
    
    
    // Date.
    $this->addFieldMapping('field_rep_date', 'field_rep_date');
    $this->addFieldMapping('field_rep_date:to')->defaultValue(0);
    $this->addFieldMapping('field_rep_date:timezone')->defaultValue(0);
    $this->addFieldMapping('field_rep_date:rrule')->defaultValue(0);
  }
  
  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    
    // Trim title length.
    if (strlen($row->title) > 254) {
      $tail = substr($row->title, 254);
      $row->title = substr($row->title, 0, 254);
      
      $message = "nid={$row->nid} Title shortened to '$row->title' (dropped '{$tail}').";
      $this->queueMessage($message);
    }
    
    // Retain nids as per:
    // https://drupal.stackexchange.com/questions/216855/preserve-nids-script-add-in-migrate-d2d
    
    // Fix formatting.
    if (!empty($row->format)) {
      if ($row->format == 4 || $row->format == 2) {
        $row->format = 'full_html';
      }
      if ($row->format == 3) { // This seems to be php.
        $row->format = 'plain_text';
      }
      if ($row->format == 1) {
        $row->format = 'filtered_html';
      }
    }
    
    // Not ready or necessary?
    try {
      $this->imageTagsToEmbedTags($row);
    } catch (Exception $e) {
    }
    
  }
  
  /**
   * Img tags embed.
   * @throws Exception
   */
  private function imageTagsToEmbedTags($row)
  {
    $body = $row->body;
    $type_detect = array(
      'UTF-8',
      'ASCII',
      'ISO-8859-1',
      'ISO-8859-2',
      'ISO-8859-3',
      'ISO-8859-4',
      'ISO-8859-5',
      'ISO-8859-6',
      'ISO-8859-7',
      'ISO-8859-8',
      'ISO-8859-9',
      'ISO-8859-10',
      'ISO-8859-13',
      'ISO-8859-14',
      'ISO-8859-15',
      'ISO-8859-16',
      'WINDOWS-1251',
      'Windows-1252',
      'Windows-1254',
    );
    $convert_from = mb_detect_encoding($body, $type_detect);
    if ($convert_from != 'UTF-8') {
      // This was not UTF-8 so report the anomaly.
      $message = "Converted from: {$convert_from}";
      $this->queueMessage($message);
    }
    
    $qp_options = array(
      'convert_to_encoding' => 'UTF-8',
      'convert_from_encoding' => $convert_from,
    );
    
    // Create query path object.
    try {
      $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
      $html .= '<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=';
      $html .= $convert_from;
      $html .= '" /></head><body>';
      $html .= $body;
      $html .= '</body></html>';
      $qp = htmlqp($html, NULL, $qp_options);
    } catch (Exception $e) {
      $error_message = $e->getMessage();
      $this->queueMessage("Failed instantiate QueryPath for HTML, Exception: {$error_message}");
    }
    
    if (!is_object($qp)) {
      throw new Exception("failed to initialize QueryPath");
    }
    
    // Non html content (plain text for example) does not get processed
    // correctly, so lets just return our body.
    $html = $qp->html();
    if (empty($html)) {
      $new_body = $body;
    } else {
      $new_body = $qp->find('body')->innerHtml();
    }
    
    $row->body = $new_body;
  }
  
  
}