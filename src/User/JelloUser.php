<?php
/**
 * @file
 * News.
 */

/*namespace JelloMigrate\User;
/*use DrupalUser6Migration;

/**
 * Migrate users to D7.
 */

class JelloUser extends DrupalUser6Migration
{
  /**
   * Constructor override.
   */
  public function __construct(array $arguments)
  {
  
    $arguments['machine_name'] = 'JelloUser';
    $arguments['role_migration'] = 'JelloRole';
    $arguments['class_name'] = 'JelloUser';
    
    parent::__construct($arguments);
    
    $this->softDependencies = array('JelloRole',);
    
    $this->addFieldMapping('is_new', NULL, FALSE)->defaultValue(TRUE);
    $this->addFieldMapping('uid', 'uid', FALSE)->defaultValue(1);
    $this->addFieldMapping('roles', 'roles', FALSE)
      ->sourceMigration('JelloRole');

    
    
  }
  
}

