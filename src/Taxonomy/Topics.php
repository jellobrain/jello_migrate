<?php
/**
 * @file
 * News.
 */


/**
 * Migrate Terms to D7.
 */
class Topics extends DrupalTerm6Migration
{
  /**
   * Constructor override.
   */
  public function __construct(array $arguments)
  {
    
    $arguments['source_vocabulary'] = '26';
    $arguments['destination_vocabulary'] = 'topics';
    $arguments['machine_name'] = 'Topics';
    
    parent::__construct($arguments);
    
    $this->addFieldMapping('tid', 'tid', FALSE);
    
    $this->addFieldMapping('field_taxonomy_image', 'taxonomy_image')
      ->sourceMigration('JelloFile');
    $this->addFieldMapping('field_taxonomy_image:file_class')->defaultValue('MigrateFileFid');
    $this->addFieldMapping('field_taxonomy_image:preserve_files', NULL, FALSE)->defaultValue(TRUE);
    $this->addFieldMapping('field_taxonomy_image:alt', NULL)->defaultValue('Image');
    $this->addFieldMapping('field_taxonomy_image:title', NULL)->defaultValue('Image');
    
    // Not mapped naturally
  
  }
  
  public function prepareRow($row)
  {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    
    $query = db_select('jello.' . 'term_data', 'td')
      ->fields('td', array('tid'))
      ->condition('td.vid', 26, '=')
      ->condition('td.tid', $row->tid, '=');
  
    // Add the taxonomy_image field from the {term_fields_term} table.
    $query->innerJoin('jello.' . 'term_fields_term', 'tf', 'td.tid=tf.tid');
    $query->addField('tf', 'taxonomy_image_fid', 'taxonomy_image');
    
    $result = $query->execute()->fetchAssoc();
  
    if (!empty($result)) {
      $row->taxonomy_image = $result['taxonomy_image'];
    }
    
    return $row;
  }
  
  
  // Migrating taxonomy terms while preserving term IDs
  public function preImport() {
    parent::preImport();
    /**
     * Also works and wanted to keep in code base:
     * $vocabs = taxonomy_vocabulary_get_names();
     * $vid = $vocabs[$this->destination->getBundle()]->vid;
     */
    $vid = 26;
    $query = $this->query();
    if ($this->getItemLimit()>0) {
      $query->range(0, $this->getItemLimit());
    }
    $results = $query->execute()->fetchAllAssoc('tid');
    foreach ($results as $tid=>$result) {
      if (!taxonomy_term_load($tid)) {
        $term = new StdClass();
        $term->tid = $tid;
        $term->name = 'Stub term: ' . $tid;
        $term->description = '';
        $term->vid = $vid;
        $status = drupal_write_record('taxonomy_term_data', $term);
      }
    }
  }
  
  /**
   * Implementation of DrupalTermMigration::query().
   *
   * @return SelectQueryExtender
   */
  protected function query() {
    // Note the explode - this supports the (admittedly unusual) case of
    // consolidating multiple vocabularies into one.
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select('term_data', 'td')
      ->fields('td', array('tid', 'name', 'description', 'weight'))
      ->condition('td.vid', 26, '=')
      ->distinct();
    
    return $query;
  }
  
  /**
   * Completion method for inserting or updating supporting data for the term object
   * into the database.  This method create/updates records for the term data, so
   * legacy data can be maintained.
   *
   * @param stdClass $term - The term object that was created
   * @param stdClass $row - The current source row from the migration sequence
   */
  public function complete(stdClass $term, stdClass $row) {
    // Data structure to hold the additional legacy field mappings.
  
    $term->field_taxonomy_image = $row->taxonomy_image;
    
    taxonomy_term_save($term);
    
  }
  
  // Clear the cached 'Stub term' titles created in preImport().
  public function postImport() {
    parent::postImport();
    cache_clear_all('*', 'cache_entity_taxonomy_term', TRUE);
  }



}

