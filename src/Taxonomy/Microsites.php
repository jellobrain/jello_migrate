<?php
/**
 * @file
 * News.
 */


/*namespace JelloMigrate\Taxonomy;
/*use DrupalTerm6Migration;

/**
 * Migrate Terms to D7.
 */
class Microsites extends DrupalTerm6Migration
{
  /**
   * Constructor override.
   */
  public function __construct(array $arguments)
  {
    
    $arguments['source_vocabulary'] = '21';
    $arguments['destination_vocabulary'] = 'microsites';
    $arguments['machine_name'] = 'Microsites';
    
    parent::__construct($arguments);
    
    $this->addFieldMapping('field_taxonomy_current_microsite', 'taxonomy_microsite_current');
    $this->addFieldMapping('tid', 'tid', FALSE);
    
    
  }
  
  public function prepareRow($row)
  {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
    
    $query = db_select('jello.' . 'term_data', 'td')
      ->fields('td', array('tid'))
      ->condition('td.vid', 21, '=')
      ->condition('td.tid', $row->tid, '=');
  
    $query->innerJoin('jello.' . 'term_fields_term', 'tf', 'td.tid=tf.tid');
    $query->addField('tf', 'taxonomy_microsite_current_value', 'taxonomy_microsite_current');
    
    $result = $query->execute()->fetchAssoc();
    
    if (!empty($result)) {
      $row->taxonomy_microsite_current = $result['taxonomy_microsite_current'];
    }
    
    return $row;
  }
  
  
  // Migrating taxonomy terms while preserving term IDs
  public function preImport() {
    parent::preImport();
    
    $vid = 21;
    $query = $this->query();
    
    if ($this->getItemLimit()>0) {
      $query->range(0, $this->getItemLimit());
    }
    
    $results = $query->execute()->fetchAllAssoc('tid');
    foreach ($results as $tid=>$result) {
      if (!taxonomy_term_load($tid)) {
        $term = new StdClass();
        $term->tid = $tid;
        $term->name = 'Stub term: ' . $tid;
        $term->description = '';
        $term->vid = $vid;
        $status = drupal_write_record('taxonomy_term_data', $term);
        
      }
    }
  }
  
  /**
   * Implementation of DrupalTermMigration::query().
   *
   * @return SelectQueryExtender
   */
  protected function query()
  {
    // Note the explode - this supports the (admittedly unusual) case of
    // consolidating multiple vocabularies into one.
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select('term_data', 'td')
      ->fields('td', array('tid', 'name', 'description', 'weight'))
      ->condition('td.vid', 21, '=')
      ->distinct();
    
    
    return $query;
  }
  
  /**
   * Completion method for inserting or updating supporting data for the term object
   * into the database.  This method create/updates records for the term data, so
   * legacy data can be maintained.
   *
   * @param stdClass $term - The term object that was created
   * @param stdClass $row - The current source row from the migration sequence
   */
  public function complete(stdClass $term, stdClass $row)
  {
    
    $term->field_taxonomy_current_microsite = $row->taxonomy_microsite_current;
  
    taxonomy_term_save($term);
    
    
  }
  
  // Clear the cached 'Stub term' titles created in preImport().
  public function postImport() {
    parent::postImport();
    cache_clear_all('*', 'cache_entity_taxonomy_term', TRUE);
  }
  
}