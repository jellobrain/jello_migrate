<?php
/**
 * @file
 * News.
 */

/*namespace JelloMigrate\Taxonomy;
/*use DrupalTerm6Migration;
/*use stdClass;

/**
 * Migrate Terms to D7.
 */
class PolicyAreas extends DrupalTerm6Migration
{
  /**
   * Constructor override.
   */
  public function __construct(array $arguments)
  {
    
    $arguments['source_vocabulary'] = '6';
    $arguments['destination_vocabulary'] = 'policy_areas';
    $arguments['machine_name'] = 'PolicyAreas';
    
    parent::__construct($arguments);
    
    $this->addFieldMapping('tid', 'tid', FALSE)
      ->defaultValue(FALSE);
    
  }
  
  
  // Migrating taxonomy terms while preserving term IDs
  public function preImport() {
    parent::preImport();
    $vocabs = taxonomy_vocabulary_get_names();
    $vid = $vocabs[$this->destination->getBundle()]->vid;
    $query = $this->query();
    if ($this->getItemLimit()>0) {
      $query->range(0, $this->getItemLimit());
    }
    $results = $query->execute()->fetchAllAssoc('tid');
    foreach ($results as $tid=>$result) {
      if (!taxonomy_term_load($tid)) {
        $term = new StdClass();
        $term->tid = $tid;
        $term->name = 'Stub term: ' . $tid;
        $term->description = '';
        $term->vid = $vid;
        $status = drupal_write_record('taxonomy_term_data', $term);
      }
    }
  }
  
  // Clear the cached 'Stub term' titles created in preImport().
  public function postImport() {
    parent::postImport();
    cache_clear_all('*', 'cache_entity_taxonomy_term', TRUE);
  }
  
  
}