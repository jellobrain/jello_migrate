<?php
/**
 * @file
 * News.
 */



/**
 * Migrate Terms to D7.
 */
class Regions extends DrupalTerm6Migration
{
  /**
   * Constructor override.
   */
  public function __construct(array $arguments)
  {
    
    $arguments['source_vocabulary'] = '10';
    $arguments['destination_vocabulary'] = 'regions';
    $arguments['machine_name'] = 'Regions';
    
    parent::__construct($arguments);
    
    $this->addFieldMapping('tid', 'tid', FALSE);
    $this->addFieldMapping('field_region_pos', 'region_pos');
    
    $this->addFieldMapping('parent', 'parent', FALSE);
    $this->addFieldMapping('parent:source_type')->defaultValue('tid');
    
  }
  
  public function prepareRow($row)
  {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }
  
    $query = db_select('jello.' . 'term_data', 'td')
      ->fields('td', array('tid'))
      ->condition('td.vid', 10, '=')
      ->condition('td.tid', $row->tid, '=');
    
    // Add the region_pos field from the {term_fields_term} table.
    $query->innerJoin('jello.' . 'term_fields_term', 'tf', 'td.tid=tf.tid');
    $query->addField('tf', 'region_pos_value', 'region_pos');
    
    $result = $query->execute()->fetchAssoc();
    
    
    if (!empty($result)) {
      $row->region_pos = $result['region_pos'];
    }
    
    return $row;
  }
    
    
    // Migrating taxonomy terms while preserving term IDs
  public function preImport() {
    parent::preImport();
    
    $vid = 10;
    $query = $this->query();
    if ($this->getItemLimit()>0) {
      $query->range(0, $this->getItemLimit());
    }
    $results = $query->execute()->fetchAllAssoc('tid');
    foreach ($results as $tid=>$result) {
      if (!taxonomy_term_load($tid)) {
        $term = new StdClass();
        $term->tid = $tid;
        $term->name = 'Stub term: ' . $tid;
        $term->description = '';
        $term->vid = $vid;
        $status = drupal_write_record('taxonomy_term_data', $term);
      }
    }
  }
  
  /**
   * This is postImport().
   */
  function postImport() {
  
    // Clear the cached 'Stub term' titles created in preImport().
    parent::postImport();
    cache_clear_all('*', 'cache_entity_taxonomy_term', TRUE);
  }
  
  /**
   * Implementation of DrupalTermMigration::query().
   *
   * @return SelectQueryExtender
   */
  protected function query() {
    $query = Database::getConnection('default', $this->sourceConnection)
      ->select('term_data', 'td')
      ->fields('td', array('tid', 'name', 'description', 'weight'))
      ->condition('td.vid', 10, '=');
    // Join to the hierarchy so we can sort on parent, but we'll pull the
    // actual parent values in separately in case there are multiples.
    $query->leftJoin('term_hierarchy', 'th', 'td.tid=th.tid');
    $query->orderBy('th.parent');
    
    
    return $query;
  }
  
  /**
   * Completion method for inserting or updating supporting data for the term object
   * into the database.  This method create/updates records for the term data, so
   * legacy data can be maintained.
   *
   * @param stdClass $term - The term object that was created
   * @param stdClass $row - The current source row from the migration sequence
   */
  public function complete(stdClass $term, stdClass $row) {
    // Data structure to hold the additional legacy field mappings
  
    $term->field_region_pos['und'][0] = $row->region_pos;
    
    taxonomy_term_save($term);
    
  }
  
}