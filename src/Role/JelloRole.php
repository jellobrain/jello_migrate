<?php

/**
 * @file
 * Make a copy of the role table. To /*use this you must create a table named
 * role_copy with the same structure as role.
 */


/**
 * Migrate roles to D7.
 */

class JelloRole extends DrupalRole6Migration
{
  public function __construct($arguments)
  {
    $arguments['machine_name'] = 'JelloRole';
    $arguments['class_name'] = 'JelloRole';
    
    parent::__construct($arguments);
    
    $this->addFieldMapping('name', 'name', FALSE);
    
  }
  
}