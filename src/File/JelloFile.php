<?php
/**
 * @file
 * News.
 */



/**
 * Migrate Files to D7.
 */

class JelloFile extends DrupalFile6Migration
{
  
  protected $legacyPath;
  
  public function __construct(array $arguments)
  {
    $arguments['machine_name'] = 'JelloFile';
  
    parent::__construct($arguments);
  
  
    $this->addFieldMapping('pathauto', NULL, FALSE)->defaultValue(0);
    $this->addFieldMapping('urlencode', NULL, FALSE)->defaultValue(TRUE);
    $this->addFieldMapping('destination_file', 'filename', FALSE);
    if (module_exists('path')) {
      $this->addFieldMapping('path', 'filepath', FALSE);
    }
  
    
  
  }

}
