<?php
/**
 * @file jello_migrate.migrate.inc
 */

/**
 * This is example code for a Drupal 6 to Drupal 7 migration. This won't actually
 * work without defining the referenced vocabularies, content types, and fields
 * on each side.
 */

/**
 * Implements hook_migrate_api().
 */
function jello_migrate_migrate_api() {
  
  $api = array(
    'api' => 2,
    'groups' =>
      array (
        'jello-user' =>
          array (
            'title' => 'JelloUser',
          ),
        'jello-file' =>
          array (
            'title' => 'JelloFile',
          ),
        'jello-taxonomy' =>
          array (
            'title' => 'JelloTaxonomy',
          ),
        'jello-taxonomy-complex' =>
          array (
            'title' => 'JelloTaxonomyComplex',
          ),
        'jello-microsite' =>
          array (
            'title' => 'JelloMicrosite',
          ),
        'jello-content' =>
          array (
            'title' => 'JelloContent',
          ),
        'jello-content-complex' =>
          array (
            'title' => 'JelloContentComplex',
          ),
      ),
    'migrations' => array(
      // JelloUser.
      'JelloRole' =>
        array (
          'class_name' => 'JelloRole',
          'description' => 'Migration of Jello Roles to D7',
          'group_name' => 'jello-user',
        ),
      'JelloUser' =>
        array (
          'class_name' => 'JelloUser',
          'description' => 'Migration of Users to D7',
          'group_name' => 'jello-user',
        ),
      // JelloFile.
      'JelloFile' =>
        array (
          'class_name' => 'JelloFile',
          'description' => 'Migration of Users to D7',
          'group_name' => 'jello-file',
        ),
      // JelloTaxonomy.
      'Featured' =>
        array (
          'class_name' => 'Featured',
          'description' => 'Migration of Featured to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'JelloTopics' =>
        array (
          'class_name' => 'JelloTopics',
          'description' => 'Migration of JelloTopics to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'Language' =>
        array (
          'class_name' => 'Language',
          'description' => 'Migration of Language to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'NewsAuthor' =>
        array (
          'class_name' => 'NewsAuthor',
          'description' => 'Migration of NewsAuthor to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'NewsSource' =>
        array (
          'class_name' => 'NewsSource',
          'description' => 'Migration of NewsSource to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'OccupationalGroups' =>
        array (
          'class_name' => 'OccupationalGroups',
          'description' => 'Migration of OccupationalGroups to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'PolicyAreas' =>
        array (
          'class_name' => 'PolicyAreas',
          'description' => 'Migration of PolicyAreas to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'PublicationAuthor' =>
        array (
          'class_name' => 'PublicationAuthor',
          'description' => 'Migration of PublicationAuthor to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'PublicationType' =>
        array (
          'class_name' => 'PublicationType',
          'description' => 'Migration of PublicationType to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'ReportAuthor' =>
        array (
          'class_name' => 'ReportAuthor',
          'description' => 'Migration of ReportAuthor to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'ResourceAuthor' =>
        array (
          'class_name' => 'ResourceAuthor',
          'description' => 'Migration of ResourceAuthor to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'ResourceTypes' =>
        array (
          'class_name' => 'ResourceTypes',
          'description' => 'Migration of ResourceTypes to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'Tags' =>
        array (
          'class_name' => 'Tags',
          'description' => 'Migration of Tags to D7',
          'group_name' => 'jello-taxonomy',
        ),
      'Theme' =>
        array (
          'class_name' => 'Theme',
          'description' => 'Migration of Theme to D7',
          'group_name' => 'jello-taxonomy',
        ),
      // JelloTaxonomyComplex.
      'Microsites' =>
        array (
          'class_name' => 'Microsites',
          'description' => 'Migration to Microsite content type from  Microsites to D7',
          'group_name' => 'jello-taxonomy-complex',
        ),
      'Regions' =>
        array (
          'class_name' => 'Regions',
          'description' => 'Migration of Regions to D7',
          'group_name' => 'jello-taxonomy-complex',
        ),
      'Topics' =>
        array (
          'class_name' => 'Topics',
          'description' => 'Migration of Topics to D7',
          'group_name' => 'jello-taxonomy-complex',
        ),
      // JelloMicrosites
      'OrgRepDb' =>
        array (
          'class_name' => 'OrgRepDb',
          'description' => 'Migration of OrgRepDb to D7',
          'group_name' => 'jello-microsite',
        ),
      // JelloContent.
      'Events' =>
        array (
          'class_name' => 'Events',
          'description' => 'Migrate groups of events to D7.',
          'group_name' => 'jello-content',
        ),
      'News' =>
        array (
          'class_name' => 'News',
          'description' => 'Migration of News to D7',
          'group_name' => 'jello-content',
        ),
      'Page' =>
        array (
          'class_name' => 'Page',
          'description' => 'Migration of Page to D7',
          'group_name' => 'jello-content',
        ),
      'PublicationsResources' =>
        array (
          'class_name' => 'PublicationsResources',
          'description' => 'Migration of PublicationsResources to D7',
          'group_name' => 'jello-content',
        ),
      // JelloContentComplex.
      'Blog' =>
        array(
          'class_name' => 'Blog',
          'description' => 'Migrate groups of blog to D7.',
          'group_name' => 'jello-content-complex',
        ),
      'DdblockNewsItem' =>
        array (
          'class_name' => 'DdblockNewsItem',
          'description' => 'Migrate groups of ddblock_news_item to D7.',
          'group_name' => 'jello-content-complex',
        ),
      'Event' =>
        array (
          'class_name' => 'Event',
          'description' => 'Migrate groups of event to D7.',
          'group_name' => 'jello-content-complex',
        ),
      'Image' =>
        array (
          'class_name' => 'Image',
          'description' => 'Migrate groups of image to D7.',
          'group_name' => 'jello-content-complex',
        ),
      'OccupationalGroupPage' =>
        array (
          'class_name' => 'OccupationalGroupPage',
          'description' => 'Migration of OccupationalGroupPage to D7',
          'group_name' => 'jello-content-complex',
        ),
      'ProgramPage' =>
        array (
          'class_name' => 'ProgramPage',
          'description' => 'Migration of ProgramPage to D7',
          'group_name' => 'jello-content-complex',
        ),
      'Publications' =>
        array (
          'class_name' => 'Publications',
          'description' => 'Migration of Publications to D7',
          'group_name' => 'jello-content-complex',
        ),
      'Reports' =>
        array (
          'class_name' => 'Reports',
          'description' => 'Migration of Reports to D7',
          'group_name' => 'jello-content-complex',
        ),
      'Resources' =>
        array (
          'class_name' => 'Resources',
          'description' => 'Migration of Resources to D7',
          'group_name' => 'jello-content-complex',
        ),
      'Specialist' =>
        array (
          'class_name' => 'Specialist',
          'description' => 'Migration of Specialist to D7',
          'group_name' => 'jello-content-complex',
        ),
    ),
  );

  /**
   * Each migration being registered takes an array of arguments, some required
   * and some optional. Start with the common arguments required by all - the
   * source_connection (connection key, set up in settings.php, pointing to
   * the Drupal 6 database), source_version (major version of Drupal), and
   * group_name (a.k.a. import job).
   */
  
  
  // TODO: DEPENDS ON ENV
  $common_arguments = array(
    'source_connection' => 'legacy',
    'destination_connection' => 'default',
    'source_version' => 6,
    'destination_version' => 7,
    'source_database' => array(
      'driver' => 'mysql',
      'database' => 'jello',
      'username' => 'jello', // Ideally this user has readonly access
      // Best practice: /*use a variable (defined by setting $conf in settings.php, or
      // with drush vset) for the password rather than exposing it in the code.
      //'password' => variable_get('example_migrate_password', ''),
      'password' => 'yourpassword',
      'host' => 'localhost',
      'prefix' => '',
    ),
    'destination_database' => array(
      'driver' => 'mysql',
      'database' => 'jello7',
      'username' => 'jello', // Ideally this user has readonly access
      // Best practice: /*use a variable (defined by setting $conf in settings.php, or
      // with drush vset) for the password rather than exposing it in the code.
      //'password' => variable_get('example_migrate_password', ''),
      'password' => 'yourpassword',
      'host' => 'localhost',
      'prefix' => '',
    ),
    'format_mappings' => array(
      '5' => 'markdown',
    ),
  );


  foreach ($api['migrations'] as $key => $migration) {
    $api['migrations'][$key] = array_merge($migration, $common_arguments);
  }
  
  
  /**
   * Migrate files table
   */
  
  $api['migrations']['JelloFile'] = $common_arguments + array(
      'class_name' => 'DrupalFile6Migration',
      'description' => t('Import Drupal 6 files'),
      'source_version' => '6',
      'user_migration' => 'JelloUser',
      'default_uid' => 1,
      // TODO: Dependent on ENV.  Will need changed in other environments.
      // This needs to be the root of the legacy site root (/) on the physical server.
      'source_dir' => '/home/ana/workspace/jello/jello/jello',
      // This needs to be the path uri.
      'destination_dir' => 'sites/default/files',
    );
  
  return $api;
}
